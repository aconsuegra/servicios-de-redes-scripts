#!/bin/sh
# Limpia pantalla
printf "\033c"
colorAzul="\e[34m"
colorRojo="\e[31m"
colorEstandar="\e[0m"
printf  "Escriba el usuario en el formato 'usuario@dominio': "
read user

printf "Escriba la contraseña:"
# No mastrar los caracteres cuando tipea.
stty -echo
read  password
cryptPassword=$user:$(doveadm pw -s CRYPT -p $password)::::::
printf "\n"
# Mostarl.
stty echo
echo "Agregue o sustituya según sea el caso la siguiente linea"
printf "${colorAzul}$cryptPassword${colorEstandar}\n"
printf "${colorRojo}en el fichero /etc/dovecot/users${colorEstandar}\n"
